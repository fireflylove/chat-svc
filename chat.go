package main

import (
	"flag"
	"fmt"

	"gitee.com/fireflylove/chat-svc/internal/config"
	"gitee.com/fireflylove/chat-svc/internal/server"
	"gitee.com/fireflylove/chat-svc/internal/svc"
	"gitee.com/fireflylove/chat-svc/template"

	"github.com/tal-tech/go-zero/core/conf"
	"github.com/tal-tech/go-zero/zrpc"
	"google.golang.org/grpc"
)

var configFile = flag.String("f", "etc/chat.yaml", "the config file")

func main() {
	flag.Parse()

	var c config.Config
	conf.MustLoad(*configFile, &c)
	ctx := svc.NewServiceContext(c)
	srv := server.NewChatServer(ctx)

	s := zrpc.MustNewServer(c.RpcServerConf, func(grpcServer *grpc.Server) {
		template.RegisterChatServer(grpcServer, srv)
	})
	defer s.Stop()

	fmt.Printf("Starting rpc server at %s...\n", c.ListenOn)
	s.Start()
}
