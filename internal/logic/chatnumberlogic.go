package logic

import (
	"context"
	"gitee.com/fireflylove/chat-svc/model"

	"gitee.com/fireflylove/chat-svc/internal/svc"
	"gitee.com/fireflylove/chat-svc/template"

	"github.com/tal-tech/go-zero/core/logx"
)

type ChatNumberLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewChatNumberLogic(ctx context.Context, svcCtx *svc.ServiceContext) *ChatNumberLogic {
	return &ChatNumberLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *ChatNumberLogic) ChatNumber(in *template.ChatNumberReq) (*template.ChatNumberRsp, error) {
	var num int64
	tx := l.svcCtx.DB.Model(&model.ChatHistory{})
	tx.Where("uid = ?", in.Id).Count(&num)

	return &template.ChatNumberRsp{Code: 0, Number: num}, nil
}
