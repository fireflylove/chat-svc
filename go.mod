module gitee.com/fireflylove/chat-svc

go 1.16

require (
	gitee.com/fireflylove/user-svc v1.1.0
	github.com/golang/protobuf v1.5.2
	github.com/jinzhu/copier v0.3.2
	github.com/tal-tech/go-zero v1.2.0
	google.golang.org/grpc v1.40.0
	gorm.io/driver/mysql v1.1.2
	gorm.io/gorm v1.21.15
)
