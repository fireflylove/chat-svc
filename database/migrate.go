package database

import (
	"gitee.com/fireflylove/chat-svc/model"
	"gorm.io/gorm"
)

func migrate(db *gorm.DB) {
	db.AutoMigrate(model.Chat{})
	db.AutoMigrate(model.ChatHistory{})
}
